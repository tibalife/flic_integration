package io.flic.pbfsample;

import android.app.Application;
import android.content.Intent;
import android.widget.Toast;

import io.flic.poiclib.*;

public class PbfSampleApplication extends Application {
	public static final String TAG = "PbfSampleApplication";

	@Override
	public void onCreate() {
		super.onCreate();

		startService(new Intent(this.getApplicationContext(), PbfSampleService.class));

		FlicManager.init(this.getApplicationContext(), "2125f7c3-0d0e-42d5-88fe-fda8765867d6", "94d6448c-22d3-4d2e-951f-f625f60f471a");

		FlicManager manager = FlicManager.getManager();

		for (FlicButton button : manager.getKnownButtons()) {
			button.connect();
			listenToButtonWithToast(button);
		}
	}

	public void listenToButtonWithToast(FlicButton button) {
		button.addEventListener(new FlicButtonAdapter() {
			@Override
			public void onButtonUpOrDown(FlicButton button, boolean wasQueued, int timeDiff, boolean isUp, boolean isDown) {
				if (isDown) {
					Toast.makeText(getApplicationContext(), "Button " + button + " was pressed", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
}
