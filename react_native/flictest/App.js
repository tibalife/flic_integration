import React, { Component } from 'react';
import { View, NativeEventEmitter } from 'react-native';
import { Button, Text, Badge } from 'native-base';
import Flic from 'react-native-flic';

class FlicExample extends Component {
    constructor(props) {
        super(props);
        this.eventListener = false;
    }

    state = {
        status: '',
        pressCount: 0,
        buttonConnected: false,
    }

    componentDidMount() {
        if (!this.eventListener) {
            const FlicEvents = new NativeEventEmitter(Flic);
            FlicEvents.addListener('FLIC', event => this.receivedEvent(event));
        }

        // Pair known buttons
        Flic.getKnownButtons('FLIC');
    }

    receivedEvent(response) {
        const { pressCount } = this.state;

        console.log('Response received:', response);

        if (response.event) {
            this.setState({ status: response.event });

            switch (response.event) {
                case 'BUTTON_PRESSED':
                    // Do whatever you want.
                    this.setState({ pressCount: pressCount + 1 });
                    break;
                case 'BUTTON_CONNECTED':
                    this.setState({ buttonConnected: true });
                    break;

                default:
            }
        }
    }

    render() {
        const { buttonConnected, status, pressCount } = this.state;

        return (
            <View style={{ flex: 1 }}>
                <Button
                    block
                    key="search-flic-buttons"
                    style={{ marginBottom: 10 }}
                    onPress={() => Flic.searchButtons('FLIC')}
                >
                    <Text>Search for Flic buttons</Text>
                </Button>

                <Button
                    block
                    key="get-flic-buttons"
                    style={{ marginBottom: 10 }}
                    onPress={() => Flic.getKnownButtons('FLIC')}
                >
                    <Text>Pair Flic buttons</Text>
                </Button>

                {buttonConnected &&
                <Badge success style={{ marginBottom: 10, alignSelf: 'center' }}>
                    <Text>Flic button Connected</Text>
                </Badge>
                }

                <Text style={{ marginTop: 10, marginBottom: 10 }}>Flic event: {status}</Text>

                {pressCount > 0 &&
                <Text style={{ fontSize: 24 }}>{pressCount} time(s) pressed</Text>
                }
            </View>
        );
    }
}

export default FlicExample;